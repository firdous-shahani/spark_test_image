package com.spark.imageupload.data

import com.google.firebase.database.PropertyName

data class ImageModel (
    @set:PropertyName("imageName")
    @get:PropertyName("imageName")
    var imageName: String = "",
    @set:PropertyName("imageUrl")
    @get:PropertyName("imageUrl")
    var imageUrl: String = ""
    )