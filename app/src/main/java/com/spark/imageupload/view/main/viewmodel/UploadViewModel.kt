package com.spark.imageupload.view.main.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.spark.imageupload.data.ImageModel
import com.spark.imageupload.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class UploadViewModel(
    private val storageReference: StorageReference,
    private val databaseReference: DatabaseReference
) : ViewModel() {

    private val _progressUpdate = MutableLiveData<Double>()
    val progressUpdate: LiveData<Double>
        get() = _progressUpdate

    private val _uploadResult = MutableLiveData<Resource<Void>>()
    val uploadResult: LiveData<Resource<Void>>
        get() = _uploadResult


    /**
     * upload image to Firebase storage and save url on realtime db
     */
    fun uploadImage(imageUrl: String?, extension: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _uploadResult.postValue(Resource.loading(null))
                val uriPath = Uri.parse("file://$imageUrl")
                val fileName =
                    storageReference.child("${System.currentTimeMillis()} . $extension")
                fileName.putFile(uriPath).addOnSuccessListener {
                    updateDatabase(fileName)
                }
                    .addOnFailureListener {
                        _uploadResult.postValue(Resource.error(it.message ?: "", null))
                    }
                    .addOnProgressListener {
                        setUploadProgress(it)
                    }
            } catch (ex: Exception) {
                _uploadResult.postValue(Resource.error(ex.message ?: "", null))
            }
        }
    }

    fun setUploadProgress(it: UploadTask.TaskSnapshot) {
        _progressUpdate.postValue(100.0 * it.bytesTransferred / it.totalByteCount)
    }

    private fun updateDatabase(snapshot: StorageReference) {
        var imageModel: ImageModel?
        snapshot.downloadUrl.addOnSuccessListener {
            imageModel = ImageModel("file${System.currentTimeMillis()}", it.toString())
            val uploadId = databaseReference.push().key
            uploadId?.let { id ->
                databaseReference.child(id).setValue(imageModel)
                    .addOnSuccessListener { result ->
                        _uploadResult.postValue(Resource.success(result))
                    }
                    .addOnFailureListener { ex ->
                        _uploadResult.postValue(Resource.error(ex.message ?: "", null))
                    }
            }
        }

    }

    fun setProgress(progress: Double) {
        _progressUpdate.value = progress
    }

    fun setUploadResult(result: Resource<Void>) {
        _uploadResult.value = result
    }
}