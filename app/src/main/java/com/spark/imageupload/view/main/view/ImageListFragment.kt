package com.spark.imageupload.view.main.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.spark.imageupload.R
import com.spark.imageupload.util.*
import com.spark.imageupload.view.main.adapter.ImageAdapter
import com.spark.imageupload.view.main.viewmodel.DownloadViewModel
import kotlinx.android.synthetic.main.common_toolbar.*
import kotlinx.android.synthetic.main.fragment_image_list.progress_circular
import kotlinx.android.synthetic.main.fragment_image_list.rv_images
import kotlinx.android.synthetic.main.fragment_image_list.txt_no_data
import kotlinx.android.synthetic.main.fragment_image_list.btn_add_photo


class ImageListFragment : Fragment() {

    private var imageAdapter: ImageAdapter? = null
    private lateinit var viewModel: DownloadViewModel
    private lateinit var networkHelper: NetworkHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_image_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        networkHelper = NetworkHelper(requireContext())
        val databaseReference = FirebaseDatabase.getInstance().getReference(UPLOADS)
        viewModel = ViewModelProviders.of(this, CustomViewModelFactory(databaseReference))
            .get(DownloadViewModel::class.java)
        if (networkHelper.isNetworkConnected()) {
            viewModel.fetchImages()
        }
        activity?.txt_header?.text = getString(R.string.photo_gallery)
        activity?.ibtn_back?.hide()
        initObserver()
        initUI()
        setListener()
    }

    private fun initObserver() {
        viewModel.images.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progress_circular.hide()
                    it.data?.let { images ->
                        if (images.isEmpty()) {
                            txt_no_data.show()
                            rv_images.hide()
                        } else {
                            txt_no_data.hide()
                            rv_images.show()
                            imageAdapter?.updateImageList(images.asReversed())
                        }
                    }
                }
                Status.LOADING -> {
                    progress_circular.show()
                    rv_images.hide()
                }
                Status.ERROR -> {
                    progress_circular.hide()
                    CommonUtils.displaySnackbar(requireActivity(), it.message, MESSAGE_TYPE_ERROR)
                }
            }
        })
    }

    private fun initUI() {
        activity?.actionBar?.title = getString(R.string.photo_gallery)
        imageAdapter = ImageAdapter(requireContext())
        rv_images.initializeGridVertical(imageAdapter, 2)
    }

    private fun setListener() {
        btn_add_photo.setOnClickListener {
            findNavController().navigate(R.id.uploadImageFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        imageAdapter = null
    }

    inner class CustomViewModelFactory(private val databaseReference: DatabaseReference) :
        ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return DownloadViewModel(databaseReference) as T
        }
    }
}