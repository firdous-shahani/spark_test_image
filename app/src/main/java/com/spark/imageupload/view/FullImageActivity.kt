package com.spark.imageupload.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.spark.imageupload.R
import com.spark.imageupload.util.IMAGE_URL
import com.spark.imageupload.util.show
import kotlinx.android.synthetic.main.activity_full_image.img_photo
import kotlinx.android.synthetic.main.common_toolbar.txt_header
import kotlinx.android.synthetic.main.common_toolbar.ibtn_back


class FullImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_image)
        initView()
        setListener()
        getBundleData()
    }

    private fun initView() {
        txt_header.text = getString(R.string.gallery__image)
        ibtn_back.show()
    }

    private fun setListener() {
        ibtn_back.setOnClickListener { finish() }
    }

    private fun getBundleData() {
        if (intent != null) {
            val imageUrl = intent.getStringExtra(IMAGE_URL)
            if(imageUrl != null) {
                Glide.with(this)
                    .load(imageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(img_photo)
            }

        }
    }
}
