package com.spark.imageupload.view.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.database.*
import com.spark.imageupload.data.ImageModel
import com.spark.imageupload.util.Resource
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import java.util.ArrayList

class DownloadViewModel(private val databaseReference: DatabaseReference) : ViewModel() {

    private val _images = MutableLiveData<Resource<List<ImageModel>>>()
    val images: LiveData<Resource<List<ImageModel>>>
        get() = _images

    fun fetchImages() {
        viewModelScope.launch {
            try {
                _images.postValue(Resource.loading(null))
                databaseReference.addValueEventListener(object : ValueEventListener {

                    override fun onCancelled(error: DatabaseError) {
                        _images.postValue(Resource.error("database Error", null))
                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        val listData: ArrayList<ImageModel> = ArrayList()
                        for (data in snapshot.children) {
                            val image = data.getValue(ImageModel::class.java)
                            image?.let { listData.add(it) }
                        }
                        _images.postValue(Resource.success(listData))
                    }
                })
            } catch (ex: Exception) {
                _images.postValue(Resource.error(ex.message ?: "", null))
                Timber.e(ex)
            }

        }
    }

    fun setImageData(data: Resource<List<ImageModel>>) {
        _images.value = data
    }
}