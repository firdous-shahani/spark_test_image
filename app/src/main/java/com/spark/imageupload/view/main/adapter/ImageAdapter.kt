package com.spark.imageupload.view.main.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.spark.imageupload.R
import com.spark.imageupload.data.ImageModel
import com.spark.imageupload.util.IMAGE_URL
import com.spark.imageupload.view.FullImageActivity
import kotlinx.android.synthetic.main.item_images.view.img_photo

class ImageAdapter(var context: Context) : RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {

    private var imageList: List<ImageModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_images, parent, false)
        return ImageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (imageList.isNotEmpty()) {
            imageList.size
        } else
            0
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.setView(imageList[position])
    }

    fun updateImageList(uploadModelList: List<ImageModel>) {
        this.imageList = uploadModelList
        notifyDataSetChanged()
    }

    inner class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { gotoFullScreenImage(layoutPosition) }
        }

        fun setView(imageModel: ImageModel) {
            Glide.with(context)
                .load(imageModel.imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(itemView.img_photo)
        }
    }

    private fun gotoFullScreenImage(layoutPosition: Int) {
        val intent = Intent(context, FullImageActivity::class.java)
        intent.putExtra(IMAGE_URL, imageList[layoutPosition].imageUrl)
        context.startActivity(intent)
    }


}