package com.spark.imageupload.view

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.view.KeyEvent
import android.webkit.MimeTypeMap
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.spark.imageupload.R
import com.spark.imageupload.util.CommonUtils
import com.spark.imageupload.util.FILE_EXTENSION
import com.spark.imageupload.util.IMAGE_FILE

import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView

import java.io.File

import com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE

class ImagePickerActivity : AppCompatActivity() {
    private var cameraImageUri: Uri? = null
    private var extension: String? = ""
    private var requestCode = 0

    companion object {
        private const val REQUEST_CAMERA_PIC = 100
        private const val REQUEST_GALLERY_PIC = 101
    }

    private val cameraImageName: String
        get() = System.currentTimeMillis().toString() + ".jpeg"

    private var galleryPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                pickImageFromGallery()
            }
        }

    private var cameraPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                takePictureFromCamera()
            }
        }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                when (requestCode) {
                    CROP_IMAGE_ACTIVITY_REQUEST_CODE -> cropImage(result.data)

                    REQUEST_CAMERA_PIC -> requestForCameraImage()

                    REQUEST_GALLERY_PIC -> requestForGalleryImage(result.data)
                    else -> finish()
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showImagePickerDialog()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun cropImage(data: Intent?) {
        val result = CropImage.getActivityResult(data)
        var imageUri: Uri? = result.uri
        if (imageUri == null) {
            imageUri = cameraImageUri
        }
        sendResultData(imageUri!!.path, extension)
    }

    private fun requestForCameraImage() {
        if (cameraImageUri != null && !TextUtils.isEmpty(cameraImageUri!!.path)) {
            extension = MimeTypeMap.getFileExtensionFromUrl(
                Uri.fromFile(File(cameraImageUri?.path ?: "")).toString()
            )
            sendToCropper(cameraImageUri, System.currentTimeMillis().toString() + "")
        }
    }

    private fun requestForGalleryImage(data: Intent?) {
        val galleryUri = data?.data
        val mime = MimeTypeMap.getSingleton()
        extension = mime.getExtensionFromMimeType(galleryUri?.let { contentResolver.getType(it) })
        sendToCropper(galleryUri, System.currentTimeMillis().toString() + "")
    }

    private fun sendResultData(imagePath: String?, extension: String?) {
        val resultIntent = Intent()
        resultIntent.putExtra(IMAGE_FILE, imagePath)
        resultIntent.putExtra(FILE_EXTENSION, extension)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    private fun showImagePickerDialog() {
        val builder = AlertDialog.Builder(this)
        val items: Array<String> = arrayOf(
            this.getString(R.string.pick_from_camera),
            this.getString(R.string.pick_from_gallery)
        )
        builder.setTitle(this.getString(R.string.image_upload_title))
        builder.setItems(items) { _, which ->
            when (which) {
                0 -> {
                    if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.CAMERA
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        takePictureFromCamera()
                    } else {
                        cameraPermissionLauncher.launch(Manifest.permission.CAMERA)
                    }
                }

                1 -> {
                    if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        pickImageFromGallery()
                    } else {
                        galleryPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
                    }
                }
                else -> {
                }
            }
        }

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setOnKeyListener(KeyResponseListener())
        dialog.setOnCancelListener(CancelDialogListener())
        dialog.show()
    }

    private fun pickImageFromGallery() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        requestCode = REQUEST_GALLERY_PIC
        resultLauncher.launch(galleryIntent)
    }

    private fun takePictureFromCamera() {
        val cameraIntent = Intent()
        cameraIntent.action = MediaStore.ACTION_IMAGE_CAPTURE
        val imageFileName = cameraImageName
        cameraImageUri = CommonUtils.getTempUri(imageFileName, this)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri)
        requestCode = REQUEST_CAMERA_PIC
        resultLauncher.launch(cameraIntent)
    }

    private fun sendToCropper(uri: Uri?, imageName: String) {
        if (uri != null) {
            val intent = CropImage.activity(uri)
                .setAspectRatio(1, 1).setFixAspectRatio(true)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setActivityTitle(getString(R.string.cropper))
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setOutputCompressQuality(50)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setOutputUri(CommonUtils.getTempUri(imageName, this))
                .getIntent(this)
            requestCode = CROP_IMAGE_ACTIVITY_REQUEST_CODE
            resultLauncher.launch(intent)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return false
    }

    private inner class CancelDialogListener : DialogInterface.OnCancelListener {
        override fun onCancel(dialog: DialogInterface) {
            finish()
        }
    }

    private inner class KeyResponseListener : DialogInterface.OnKeyListener {
        override fun onKey(dialog: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
            return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
                finish()
                true
            } else {
                false
            }
        }
    }

}
