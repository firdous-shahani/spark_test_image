package com.spark.imageupload.view.main.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.spark.imageupload.R
import com.spark.imageupload.util.CommonUtils
import com.spark.imageupload.util.FILE_EXTENSION
import com.spark.imageupload.util.IMAGE_FILE
import com.spark.imageupload.util.IMAGE_URL
import com.spark.imageupload.util.MESSAGE_TYPE_ERROR
import com.spark.imageupload.util.MESSAGE_TYPE_SUCCESS
import com.spark.imageupload.util.NetworkHelper
import com.spark.imageupload.util.Status
import com.spark.imageupload.util.UPLOADS
import com.spark.imageupload.util.hide
import com.spark.imageupload.util.show
import com.spark.imageupload.view.ImagePickerActivity
import com.spark.imageupload.view.main.viewmodel.UploadViewModel
import kotlinx.android.synthetic.main.common_toolbar.txt_header
import kotlinx.android.synthetic.main.common_toolbar.ibtn_back
import kotlinx.android.synthetic.main.fragment_upload_image.btn_import_photo
import kotlinx.android.synthetic.main.fragment_upload_image.progressbar
import kotlinx.android.synthetic.main.fragment_upload_image.txt_percentage
import kotlinx.android.synthetic.main.fragment_upload_image.btn_upload_photo
import kotlinx.android.synthetic.main.fragment_upload_image.img_photo
import java.io.File

class UploadImageFragment : Fragment() {

    private var imagePath: String? = null
    private var extension: String? = null
    private lateinit var viewModel: UploadViewModel
    private lateinit var networkHelper: NetworkHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (savedInstanceState != null) {
            if (savedInstanceState.getString(IMAGE_URL) != null) {
                imagePath = savedInstanceState.getString(IMAGE_URL)
                extension = savedInstanceState.getString(FILE_EXTENSION)
                setImage()
            }
        }
        return inflater.inflate(R.layout.fragment_upload_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        networkHelper = NetworkHelper(requireContext())
        val databaseReference = FirebaseDatabase.getInstance().getReference(UPLOADS)
        val storageReference = FirebaseStorage.getInstance().getReference(UPLOADS)
        viewModel =
            ViewModelProviders.of(this, CustomViewModelFactory(databaseReference, storageReference))
                .get(UploadViewModel::class.java)
        activity?.txt_header?.text = getString(R.string.upload_image)
        activity?.ibtn_back?.show()
        setListener()
        initObserver()
    }

    private fun initObserver() {
        viewModel.progressUpdate.observe(viewLifecycleOwner, Observer {
            val percentage = "${it.toInt()}%"
            if (!progressbar.isVisible) {
                progressbar.show()
            }
            progressbar.progress = it.toInt()
            txt_percentage.text = percentage
        })

        viewModel.uploadResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressbar.hide()
                    CommonUtils.displaySnackbar(
                        requireActivity(),
                        getString(R.string.image_uploaded_success),
                        MESSAGE_TYPE_SUCCESS
                    )
                    val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed({
                        progressbar.visibility = View.GONE
                        txt_percentage.text = ""
                        requireActivity().onBackPressed()
                    }, 1000)
                }
                Status.LOADING -> {
                    progressbar.show()
                }
                Status.ERROR -> {
                    progressbar.hide()
                }
            }
        })
    }

    private fun setListener() {
        btn_upload_photo.setOnClickListener { uploadNewPhoto() }
        btn_import_photo.setOnClickListener { addPhoto() }
    }

    private fun uploadNewPhoto() {
        if (imagePath != null) {
            uploadImage()
        }
    }

    private fun uploadImage() {
        if (networkHelper.isNetworkConnected()) {
            if (imagePath != null && extension != null) {
                viewModel.uploadImage(imagePath, extension)
            } else {
                CommonUtils.displaySnackbar(
                    requireActivity(),
                    getString(R.string.empty_image_error),
                    MESSAGE_TYPE_ERROR
                )
            }
        }
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->

            if (result.resultCode == Activity.RESULT_OK) {
                if (result.data != null) {
                    imagePath = result.data?.getStringExtra(IMAGE_FILE)
                    extension = result.data?.getStringExtra(FILE_EXTENSION)
                    setImage()
                }
            }
        }

    private fun addPhoto() {
        val intent = Intent(requireActivity(), ImagePickerActivity::class.java)
        resultLauncher.launch(intent)
    }

    private fun setImage() {
        Glide.with(requireActivity())
            .load(File(imagePath ?: ""))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(img_photo)
    }

    override fun onSaveInstanceState(bundle: Bundle) {
        super.onSaveInstanceState(bundle)
        bundle.putString(IMAGE_URL, imagePath)
        bundle.putString(FILE_EXTENSION, extension)
    }

    inner class CustomViewModelFactory(
        private val databaseReference: DatabaseReference,
        private val storageReference: StorageReference
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return UploadViewModel(storageReference, databaseReference) as T
        }
    }

}