package com.spark.imageupload.util

import android.app.Activity
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.material.snackbar.Snackbar
import com.spark.imageupload.BuildConfig
import com.spark.imageupload.R
import timber.log.Timber
import java.io.File
import java.io.IOException

object CommonUtils {

    fun getTempUri(temp: String, activity: Activity): Uri {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Uri.fromFile(getTempFile(temp, activity))
        } else {
            getFileUriForNougat(activity, getTempFile(temp, activity))
        }
    }

    private fun getFileUriForNougat(activity: Activity, file: File?): Uri {
        return FileProvider.getUriForFile(
            activity, BuildConfig.APPLICATION_ID + ".provider",
            file!!
        )
    }

    private fun getTempFile(temp: String, activity: Activity): File? {
        return if (isSDCARDMounted) {
            val f = if (activity.externalCacheDir != null) {
                File(activity.externalCacheDir, temp)
            } else {
                File(activity.cacheDir, temp)
            }
            try {
                val fileCreated = f.createNewFile()
                if (fileCreated) {
                    Timber.d("file is created")
                } else {
                    Timber.d("file is not created")
                }
            } catch (e: IOException) {
                Timber.e(e)
            }
            f
        } else {
            null
        }
    }

    private val isSDCARDMounted: Boolean
        get() {
            val status = Environment.getExternalStorageState()
            return status == Environment.MEDIA_MOUNTED
        }


    fun displaySnackbar(
        activity: Activity?,
        message: String?,
        messageType: Int
    ) {
        if (activity != null) {
            var view: View? = activity.window.decorView.rootView ?: return
            try {
                view =
                    (activity.findViewById<View>(R.id.content) as ViewGroup).getChildAt(
                        0
                    )
            } catch (e: Exception) {
                Timber.d(e)
            }
            val snackbar = Snackbar.make(view!!, message!!, Snackbar.LENGTH_SHORT)
            val tvSnackBar = snackbar.view
                .findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
            tvSnackBar.textAlignment = View.TEXT_ALIGNMENT_CENTER
            when (messageType) {
                MESSAGE_TYPE_ERROR -> tvSnackBar.setBackgroundColor(
                    ContextCompat.getColor(activity, R.color.red_color)
                )
                MESSAGE_TYPE_SUCCESS -> tvSnackBar.setBackgroundColor(
                    ContextCompat.getColor(activity, R.color.green_color)
                )
                else -> tvSnackBar.setBackgroundColor(
                    ContextCompat.getColor(activity, R.color.green_color)
                )
            }
            snackbar.view.setPadding(0, 0, 0, 0)
            snackbar.show()
        }
    }

}