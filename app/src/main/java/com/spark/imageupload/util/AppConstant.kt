package com.spark.imageupload.util

const val IMAGE_FILE = "IMAGE_FILE"
const val FILE_EXTENSION = "FILE_EXTENSION"
const val IMAGE_URL = "IMAGE_URL"
const val UPLOADS = "uploads"
const val MESSAGE_TYPE_SUCCESS = 100
const val MESSAGE_TYPE_ERROR = 200