package com.spark.imageupload.util

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView


fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun RecyclerView.initializeGridVertical(adapter: RecyclerView.Adapter<*>?, gridCount: Int = 1) {
    setAdapter(adapter)
    layoutManager = GridLayoutManager(context, gridCount, GridLayoutManager.VERTICAL, false)
}

