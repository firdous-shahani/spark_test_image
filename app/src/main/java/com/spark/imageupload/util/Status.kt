package com.spark.imageupload.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}