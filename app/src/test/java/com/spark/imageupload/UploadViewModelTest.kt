package com.spark.imageupload

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.spark.imageupload.util.Resource
import com.spark.imageupload.utils.TestCoroutineRule
import com.spark.imageupload.view.main.viewmodel.UploadViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class UploadViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: UploadViewModel

    @Mock
    private lateinit var databaseReference: DatabaseReference

    @Mock
    private lateinit var storageReference: StorageReference

    @Mock
    private lateinit var progressObserver: Observer<Double>

    @Mock
    private lateinit var uploadResultObserver: Observer<Resource<Void>>

    @Mock
    private lateinit var uploadResult: Resource<Void>

    @Mock
    private lateinit var uploadTask: UploadTask.TaskSnapshot


    private val imageUrl: String = "dummy url"
    private val extension = "jpg"
    private val progress = 50.0

    @Before
    fun setup() {
        viewModel = UploadViewModel(storageReference, databaseReference)
    }

    @Test
    fun uploadProgressTest() {
        viewModel.progressUpdate.observeForever(progressObserver)
        viewModel.setProgress(progress)
        viewModel.setUploadProgress(uploadTask)
        verify(progressObserver).onChanged(progress)
    }

    @Test
    fun uploadImageSuccessResponse() {
        viewModel.uploadResult.observeForever(uploadResultObserver)
        viewModel.setUploadResult(uploadResult)
        viewModel.uploadImage(imageUrl, extension)
        verify(uploadResultObserver).onChanged(uploadResult)
    }


    @Test
    fun uploadImageErrorsResponse() {
        viewModel.uploadResult.observeForever(uploadResultObserver)
        viewModel.setUploadResult(Resource.error("error", null))
        viewModel.uploadImage(imageUrl, extension)
        verify(uploadResultObserver).onChanged(Resource.error("error", null))
    }
}