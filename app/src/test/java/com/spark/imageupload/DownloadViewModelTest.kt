package com.spark.imageupload

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.spark.imageupload.data.ImageModel
import com.spark.imageupload.util.Resource
import com.spark.imageupload.utils.TestCoroutineRule
import com.spark.imageupload.view.main.viewmodel.DownloadViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DownloadViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: DownloadViewModel

    @Mock
    private lateinit var responseData: Resource<List<ImageModel>>

    @Mock
    private lateinit var dataSnapshot: DataSnapshot

    @Mock
    private lateinit var databaseError: DatabaseError

    @Mock
    private lateinit var databaseReference: DatabaseReference

    @Captor
    private lateinit var argumentCaptor: ArgumentCaptor<ValueEventListener>

    @Mock
    private lateinit var apiObserver: Observer<Resource<List<ImageModel>>>


    @Before
    fun setup() {
        viewModel = DownloadViewModel(databaseReference)
    }

    @Test
    fun downloadImageSuccessTest() {
        testCoroutineRule.runBlockingTest {
            viewModel.images.observeForever(apiObserver)
            viewModel.setImageData(responseData)
            viewModel.fetchImages()
            Mockito.verify(databaseReference).addValueEventListener(argumentCaptor.capture())
            argumentCaptor.value.onDataChange(dataSnapshot)
            Mockito.verify(apiObserver).onChanged(responseData)
        }
    }

    @Test
    fun downloadImageFailureTest() {
        viewModel.images.observeForever(apiObserver)
        viewModel.fetchImages()
        Mockito.verify(databaseReference).addValueEventListener(argumentCaptor.capture())
        argumentCaptor.value.onCancelled(databaseError)
        Mockito.verify(apiObserver).onChanged(Resource.error("database Error", null))
    }

}