package com.spark.imageupload

import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.spark.imageupload.view.FullImageActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4 ::class)
class FullImageActivityUITest {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(FullImageActivity::class.java)

    @Test
    fun activityLaunchesSuccessfully(){
        launch(FullImageActivity::class.java)
    }

    @Test
    fun checkImageVisible(){
        onView(withId(R.id.img_photo)).check(matches(isDisplayed()))
    }
    @Test
    fun checkBackButtonVisible(){
        onView(withId(R.id.ibtn_back)).check(matches(isDisplayed()))
    }
    @Test
    fun checkHeaderTextVisible(){
        onView(withId(R.id.txt_header)).check(matches(isDisplayed()))
    }
    @Test
    fun backButtonClick(){
        onView(withId(R.id.ibtn_back)).perform(click())
    }
}