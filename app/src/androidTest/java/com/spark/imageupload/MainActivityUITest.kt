package com.spark.imageupload

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.fragment.app.testing.withFragment
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.spark.imageupload.view.ImagePickerActivity
import com.spark.imageupload.view.main.MainActivity
import com.spark.imageupload.view.main.view.ImageListFragment
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before


@RunWith(AndroidJUnit4 ::class)
class MainActivityUITest {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setup() {
        Intents.init()
    }

    @After
    fun tearDown(){
        Intents.release()
    }


    @Test
    fun activityLaunchesSuccessfully(){
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun checkAddButtonVisibility(){
        onView(withId(R.id.btn_add_photo)).check(matches(isDisplayed()))
    }
    @Test
    fun checkRecyclerViewVisibility(){
        onView(withId(R.id.rv_images)).check(matches(isDisplayed()))
    }

    @Test
    fun checkBackButtonVisible(){
        onView(withId(R.id.ibtn_back)).check(matches(not(isDisplayed())))
    }
    @Test
    fun checkHeaderTextVisible(){
        onView(withId(R.id.txt_header)).check(matches(isDisplayed()))
    }

    @Test
    fun testNavigationToUploadFragment(){
        onView(withId(R.id.btn_add_photo)).perform(click())
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        val imageListScenario = launchFragmentInContainer<ImageListFragment>()

        imageListScenario.withFragment {
            navController.setGraph(R.navigation.nav_main)
            Navigation.setViewNavController(this.requireView(), navController)
            navController.navigate(R.id.uploadImageFragment)
        }
    }

    @Test
    fun openGalleryCameraOnButtonClick(){
        onView(withId(R.id.btn_add_photo)).perform(click())
        onView(withId(R.id.btn_import_photo)).perform(click())
        val resultIntent = Intent()
        resultIntent.putExtra("ImagePath","somePath")
        resultIntent.putExtra("Extension","someExtension")
        val result = Instrumentation.ActivityResult(Activity.RESULT_OK,resultIntent)
        Intents.intending(IntentMatchers.toPackage(ImagePickerActivity::class.java.name)).respondWith(result)
    }

}